//use std::env;
use std::fs::File;
use std::io::{BufRead, BufReader};
use std::env;
use std::process;

fn main() {
    let unprocessed_args: Vec<String> = env::args().collect();
    
    let args = process_args(unprocessed_args);
    
    if args.help {
        println!(
            "pip-diff is a simple tool for determining which of your python \n\
            packages differ from one another in different environments. \n\
            \n\
            pip-diff expects that you have two files - each one should be the\n\
            result of running `pip freeze` in each of the environments that you\n\
            are comparing.\n\
            pip-diff expects the paths of these files to be its 2 arguments."
        );
    }

    let printer = Printer{ verbose_active: args.verbose };
    printer.verbose("pip-diff running in verbose mode".to_string());


    printer.verbose(format!("reading file_a - \'{}\'", args.file_a_path.clone()));
    let file_a = read_file(args.file_a_path.clone());
    println!("{:#?}", file_a);

    printer.verbose(format!("reading file_a - \'{}\'", args.file_a_path.clone()));
    let file_b = read_file(args.file_b_path.clone());
    println!("{:#?}", file_b);
}

fn read_file(file_path: String) -> Vec<String> {
    // read_file is a simple function to read a file in and convert it into lines.
    // Intentionlly doesn't stream input - this would be overengineering for
    // comparing pip files which I'm yet to see break ~200 lines on sizable
    // projects.
    let reader = BufReader::new(File::open(file_path).expect("Cannot read file {}"));
    
    let mut lines: Vec<String> = [].to_vec();

    for line in reader.lines() {
        lines.push(line.unwrap());
    }

    return lines;
}

#[derive(Debug)]
struct ProcessedArgs {
    file_a_path: String,
    file_b_path: String,
    verbose: bool,
    help: bool,
}

fn process_args(args: Vec<String>) -> ProcessedArgs {
    let mut verbose = false;
    let mut help = false;
    let mut file_a_path = "".to_string(); 
    let mut file_b_path = "".to_string();

    for arg in args {
        if arg == "-h" || arg == "--help" || arg == "help" {
            help = true;
            continue;
        }

        if arg == "-v" || arg == "--verbose" || arg == "verbose" {
            verbose = true;
            continue;
        }

        if arg.ends_with("/pip-diff") {
            continue;
        }

        if file_a_path == "" {
            file_a_path = arg.clone();
            continue;
        }

        if file_b_path == "" {
            file_b_path = arg.clone();
            continue;
        }
    }

    if file_b_path == "" {
        // only need to chack file_b_path, because <2 non-flag arguments will result in this
        println!("pip-diff expects two filepath arguments. \
            eg. `pip-diff example1.txt example2.txt`");
        process::exit(1);
    }

    return ProcessedArgs{
        verbose: verbose,
        help: help,
        file_a_path: file_a_path,
        file_b_path: file_b_path,
    }
}

trait Verbose {
    fn verbose(&self, input: String);
}


struct Printer {
    verbose_active: bool,
}

impl Verbose for Printer {
    fn verbose(&self, input: String) {
        if !self.verbose_active {
            return;
        }

        println!("{}", input);
    }
}
