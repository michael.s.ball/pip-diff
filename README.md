# pip-diff

pip-diff is a simple tool that has come running into issues trying to run the same Python projects in different environments, and running into versioning errors. It takes two files that are the output of running `pip freeze` in each of the environments and comparing each of the packages that have been installed, and determining at which packages differ and (where possible) at what Semantic Versioning those packages diverge.

It's written in Rust because A) I'm trying to learn Rust, and thought this would be a decent first project for it, and B) a Python tool written in Rust just amuses me.

## Usage:

### Installation:
(install instuctions to come when I figure out Rust packaging)

### Running pip-diff:
1. Install pip-diff, as per the instructions above.
1. save the results of `pip freeze` in each of the environments you're trying to compare into files on the same system as pip-diff.
1. In the CLI run `pip-diff ./path/to/file_1.txt ./path/to/file_2.txt` and enjoy.

Additional flags:
* `-v`, `--verbose`, or `verbose` will generate a bunch of ~~useless~~ output about what pip-diff is doing at each step.
* `-h`, `--help`, or `help` will spit out a brief summary of what pip did does, and how it expects to be used.

## TODOs:
* Actually finish the tool.
	* Break up each line, accounting for line type
	* Make a map of each for X-not-in-y comparison
	* Create Major/minor/patch/lower diff lists
	* Create commit diff list
	* Print results (only print relevant levels to minimise visual clutter)
* Crate up helper functions to tidy up code.
* Figure out rust installation/packaging
* Update install instructions
